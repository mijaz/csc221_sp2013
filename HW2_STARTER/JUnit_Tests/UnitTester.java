import static org.junit.Assert.*;

import java.io.File;
import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class UnitTester {
	
	Transaction t;
	DataManager d;
	@Before
	public void setUp() throws ParseException
	{
	//	t = new Transaction(123456789, Transaction.sdf.parse("2/15/13"), 235.75);
		File f = new File("./transactionlog.xml");
		d = new DataManager(f);
		
		if(d.transactionList.size() > 0)
			t = d.transactionList.get(0);
		else
			t = null;
	}
	
	@Test
	public void testWillSucceed()
	{
		//as long as we don't make any assert statements, this test can't possibly fail
		assertEquals(3.5, 3.5, 0.000001);

	}
	
	@Test
	public void setAmountTest()
	{
		t.setAmount(3.50);
		assertEquals(3.5, t.getAmount(), 0.000001);
	}
	
	@Test
	public void setAccountTest()
	{
		t.setAccount(123456789);
		assertEquals(123456789, t.getAccount(), 0.000001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void accountExceptionTest()
	{
		t.setAccount(0);
	}
	
	@Test
	public void dataManagerMeanTest()
	{
		d.printList();
		assertEquals(31.826667, d.getMean(), 0.00001);
		
		
	}
}


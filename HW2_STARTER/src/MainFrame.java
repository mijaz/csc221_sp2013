import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class MainFrame extends JFrame{

	DataManager data;
	DisplayPanel display;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Runnable r = new Runnable(){
			@Override
			public void run()
			{
				new MainFrame();
			}
		};
		
		javax.swing.SwingUtilities.invokeLater(r);
	}
	
	public MainFrame(){
		
		super("Transaction Reader");
	//	setSize(500,500);
		setBounds( 100,
				100,
				500,
				250);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(Color.white);
		
		display = new DisplayPanel();
		JButton fChooseButton = new JButton("Select a file");
		
		/* TODO:  this action listener triggers a file chooser.  You need to pass the selected
		 * file to DataManager and set the display panel with the results.
		 */
		fChooseButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser(".");
				 int returnVal = fc.showOpenDialog(MainFrame.this);

				 if (returnVal == JFileChooser.APPROVE_OPTION) 
				 {
			            File file = fc.getSelectedFile();    
			            System.out.println("Chose "+file);
				 } 
				 else 
				 {
					 System.out.println("canceled");
			     }
			}
		});
		
		/* TODO give clearButton an action listener that clears the display panel */
		JButton clearButton = new JButton("Clear");
		
		
		JPanel p = new JPanel();
		p.add(fChooseButton);
		p.add(clearButton);
		
		//since we're adding to the default content pane, it already has a BorderLayout manager.
		//Adding the button panel to the north area causes it to be rendered as small as possible 
		//while still displaying the buttons.
		add( p, BorderLayout.NORTH);
		//the display panel gets added to the center region, where it will be expanded fill remaining 
		//area in the parent frame.
		add(display);
		//pack();
		
		

	}
	
	

}

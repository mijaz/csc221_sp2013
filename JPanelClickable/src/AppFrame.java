import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class AppFrame extends JFrame {

	ArrayList<Shape> theShapes;
	
	public AppFrame()
	{
		super();
		
		theShapes = new ArrayList<Shape>();
		
		setSize(500,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		final ShapePanel p = new ShapePanel(theShapes);
		this.add(p, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		
		buttonPanel.setLayout(new GridLayout(2,1));
		buttonPanel.setBackground(Color.white);
		
		JRadioButton b1 = new JRadioButton("Circle");
		JRadioButton b2 = new JRadioButton("Square");
		
		b1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				p.setDrawShape("circle");
			}
			
		});
		
		b2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				p.setDrawShape("square");
			}
			
		});
		
		ButtonGroup radioGroup = new ButtonGroup();
		radioGroup.add(b1);
		radioGroup.add(b2);
		buttonPanel.add(b1, 0);
		buttonPanel.add(b2, 1);
		
		
		b1.setBackground(Color.gray);

		b2.setBackground(Color.gray);
		buttonPanel.add(b1);
		this.add(buttonPanel, BorderLayout.EAST);
		validate();
	}
}

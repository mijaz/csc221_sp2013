import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Constructor;
import java.util.ArrayList;

import javax.swing.JPanel;


public class ShapePanel extends JPanel{
	
	ArrayList<Shape> shapes;
	String shapeToAdd = "circle";

	MouseListener ml = new MouseListener(){

		@Override
		public void mouseClicked(MouseEvent e) {
		
			
			int x = e.getX();
			int y = e.getY();
			
			
			if(shapeToAdd == "circle")
			{
				shapes.add(new Circle(x, y, 20));
			}
			else if(shapeToAdd == "square")
			{
				shapes.add(new Square(x,y,20));
			}
		repaint();
		
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Mouse Pressed");
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			System.out.println("Mouse Released");
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			System.out.println("Mouse Entered");
				
		}

		@Override
		public void mouseExited(MouseEvent e) {
			System.out.println("Mouse Out");
			
		}
		
	};
	public ShapePanel( ArrayList<Shape> shapes)
	{
		super();
		this.shapes = shapes;
		this.setBackground(Color.white);
		this.addMouseListener(ml);
	}
	
	public void setDrawShape(String shape)
	{
		this.shapeToAdd = shape;
	}

	
	@Override public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		for(Shape s: shapes)
		{
			s.draw(g);
		}
	}
}

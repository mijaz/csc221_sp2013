import java.awt.Graphics;


public class Square extends Shape {

	public Square(int x, int y, int height) {
		super(x, y, height, height);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Graphics g) {
		g.fillRect(x, y, width, height);
	}

}

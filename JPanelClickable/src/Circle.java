import java.awt.Graphics;


public class Circle extends Shape {

	public Circle(int x, int y, int width, int heigth) {
		super(x, y, width, width);
	}
	
	public Circle(int x, int y, int diameter) {
		super(x, y, diameter, diameter);
	}

	@Override
	public void draw(Graphics g) {
		g.fillOval(x, y, width, height);
	}

}

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

//This class inherits from JFrame, and will be used to show the main window of our application
//But it also implements the ActionListener interface, indicating it will respond to things like
//button clicks.  We must provide an implementation of the actionPerformed() method, which is where
//we define the apps response to those button clicks
public class MyFrame extends JFrame implements ActionListener{

	//We declare the components we will add to this JFrame as instance variables
	//so we can store references to the objects once they are constructed that
	//will be available to all methods in the class.  This is important because we
	//will need to interact with these objects in the actionPerformed method as well as in the constructor.
	MyDrawPanel panel ;
	JButton button1, button2, grow, shrink, changeText;
	JPanel blank_panel, button_panel;
	JLabel text;
	JTextField tField;
	int count=0;
	
	//Here is our constructor, whose first order of business is to call the super constructor
	public MyFrame()  {
		super();
		
		//we instanciate the text field, the constructor takes the length in number of characters
		//that length determines the visible size, not the maximum number of character allowed.  More than
		//10 characters can be entered, but only 10 characters will be visible at a time.
		tField = new JTextField(10);
		
		//JLabels let us add text to a component like a JPanel or JFrame
		text = new JLabel("Hello from the GUI ");
		
		//Now we instanciate a few buttons and provide the text we want them to be labeled with
		button1 = new JButton("Add Panel");
		button2 = new JButton("Remove Panel");
		grow = new JButton("grow");
		shrink = new JButton("Shrink");
		changeText = new JButton("Change Text");
		
		//Now we instanciate and initialize a JPanel we will use for the lower/SOUTH area of the window
		button_panel = new JPanel();
		button_panel.setBackground(  Color.WHITE);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		//This JFrame will use the BorderLayout manager which helps us position components
		//in a way that responds well if the window is resized byt the user.  check out 
		//  http://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html
		//for more info on Layout Managers
		setLayout(new BorderLayout());
		
		setSize(400, 400);
		
		//we add our GUI components to this panel
		button_panel.add(text);
		button_panel.add(tField);
		button_panel.add(button1);
		button_panel.add(button2);
		button_panel.add(grow);
		button_panel.add(shrink);
		button_panel.add(changeText);
		
		//now we register the components that should be able to respond to events like mouse clicks
		button1.addActionListener(this);
		tField.addActionListener(this);
		grow.addActionListener(this);
		shrink.addActionListener(this);
		
		changeText.addActionListener(
				new ActionListener(){

						@Override
						public void actionPerformed(ActionEvent arg0) {
							panel.text = tField.getText();
							repaint();
						}
				}
				);
		button2.addActionListener(this);
	
		//this blank panel is just an empty white Panel we use as a place holder
		blank_panel = new JPanel();
		blank_panel.setBackground(Color.white);

		//setting preferred size makes this otherwise empty panel take up space
		//when it's containing JFrame is sized
		blank_panel.setPreferredSize(new Dimension(400,400));
	
		//we add the blank panel.  Since we're using BorderLayout and did not specify an area,
		//the panel will be added to the JFrame's CENTER area
		add(blank_panel);	
	
		//Now we expilicitly add the button panel to the JFrame's SOUTH area
		add(button_panel, BorderLayout.SOUTH);
		
		//calling pack() shrinks the JFrame to the smallest size that will still show
		//all the components we've added
		pack();

		setVisible(true); //we need to make this JFrame visible
	} 

	//events on any components we've registered to listen for actionevents
	//will get passed here by the system
	@Override
	public void actionPerformed(ActionEvent e) {
		
		//we call getSoure on the ActionEvent object to determine which component generated the event
		//i.e., which component got clicked
		if(e.getSource()==button1)
			{
			if(panel==null)
			panel = new MyDrawPanel();
			
			remove(blank_panel);
			add(panel);
			
			//If we add or remove components, we must call validate, which will force the
			//Layout Manager to arrange everything nicely.
			validate();
			
			//If we are changing components that do some graphics, we call repaint() which will force the system
			//to rerender all graphic components
			repaint();
			}
		else if(e.getSource()==tField)
		{
			//we're not doing anything here except showing that we can respond to events
			//other than mouse clicks.  When running the applicaton, if you type in the text field and
			//press enter, you should see the message below get printed on the console.
			System.out.println("Ev fired");
		}
		else if (e.getSource() == grow)
		{
			//here we increase the radius, then call repaint() so our changes show up
			panel.radius+=10;;
 
			repaint();
		}
		else if(e.getSource() == shrink)
		{
			//here we decrease the radius and call repaint()
			panel.radius -= 10;
			repaint();
		}
	/*	else if(e.getSource() == changeText)
		{
			//here we retrieve text the user entered in the text field,
			//then set the panel.text field to that value
			panel.text = tField.getText();
			
			//MyDrawPanel will paint this text, this is a way we can add text to a window instead of using a JLabel
			//we must call repaint() since this text is being drawn graphically rather than being added as a component
			repaint();
		}*/
		else  //The user must have clicked the remove button.
		{
			//so we take away the main panel and put the blank panel back and call repaint()
			//so the graphic components are re-rendered
			remove(panel);
			add(blank_panel); 
			repaint();
		}
	}



}

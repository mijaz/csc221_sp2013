


public class MainApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//we instanciate a new MyFrame onject.  The rest of the window
		//setup is defined in the MyFrame constructor
		MyFrame application  = new MyFrame();
        
	
		//The above code will work almost all the time, but very rarely, will 
		//fail due to a synchronization issue.  Using the pattern below corrects this issue,
		//and you may see it in examples on the web or in the book
		//we will go into detail abot what's going on here when we discuss multithreading
		/*
		Runnable r = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				MyFrame application  = new MyFrame();
			}
			
		};
		
		SwingUtilities.invokeLater(r);
		*/
	}

}
